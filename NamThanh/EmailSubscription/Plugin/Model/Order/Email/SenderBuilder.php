<?php


namespace NamThanh\EmailSubscription\Plugin\Model\Order\Email;

use Magento\Framework\Mail\Template\TransportBuilder;
use Magento\Sales\Model\Order\Email\Container\Template;
use Magento\Catalog\Api\ProductRepositoryInterface as ProductRepository;

/**
 * Class SenderBuilder
 * @package NamThanh\EmailSubscription\Plugin\Model\Order\Email
 */
class SenderBuilder
{
    /**
     * @var TransportBuilder
     */
    private $transportBuilder;

    /**
     * @var \Magento\Sales\Model\Order\Email\Container\OrderIdentity
     */
    private $identityContainer;

    /**
     * @var Template
     */
    private $templateContainer;

    /**
     * @var ProductRepository
     */
    private $productRepository;

    /**
     * SenderBuilder constructor.
     * @param Template $templateContainer
     * @param TransportBuilder $transportBuilder
     * @param \Magento\Sales\Model\Order\Email\Container\OrderIdentity $identityContainer
     * @param ProductRepository $productRepository
     */
    public function __construct(
        Template $templateContainer,
        TransportBuilder $transportBuilder,
        \Magento\Sales\Model\Order\Email\Container\OrderIdentity $identityContainer,
        ProductRepository $productRepository
    ) {
        $this->transportBuilder = $transportBuilder;
        $this->identityContainer = $identityContainer;
        $this->templateContainer = $templateContainer;
        $this->productRepository = $productRepository;
    }

    /**
     * @param \Magento\Sales\Model\Order\Email\SenderBuilder $subject
     * @param \Closure $proceed
     * @throws \Magento\Framework\Exception\LocalizedException
     * @throws \Magento\Framework\Exception\MailException
     */
    public function aroundSend(
        \Magento\Sales\Model\Order\Email\SenderBuilder $subject,
        \Closure $proceed
    ) {
        /** @var \Magento\Sales\Model\Order $order */

        if ($this->templateContainer->getTemplateId() !== 'sales_email_order_template') {
            return $proceed();
        }
        $order = $this->templateContainer->getTemplateVars()['order'];
        $orderItems =$order->getAllItems();
        foreach ($orderItems as $orderItem) {
            $productId = $orderItem->getProductId();
            $product = $this->productRepository->getById($productId);
            if (!$product->getSubscription()) {
                return $proceed();
            }
            break;
        }
        $this->configureEmailTemplate();

        $this->transportBuilder->addTo(
            $this->identityContainer->getCustomerEmail(),
            $this->identityContainer->getCustomerName()
        );

        $copyTo = $this->identityContainer->getEmailCopyTo();

        if (!empty($copyTo) && $this->identityContainer->getCopyMethod() == 'bcc') {
            foreach ($copyTo as $email) {
                $this->transportBuilder->addBcc($email);
            }
        }

        $transport = $this->transportBuilder->getTransport();
        $transport->sendMessage();
    }

    /**
     * @throws \Magento\Framework\Exception\MailException
     */
    private function configureEmailTemplate()
    {
        $this->transportBuilder->setTemplateIdentifier('new_order_for_subscription');
        $this->transportBuilder->setTemplateOptions($this->templateContainer->getTemplateOptions());
        $this->transportBuilder->setTemplateVars($this->templateContainer->getTemplateVars());
        $this->transportBuilder->setFromByScope(
            $this->identityContainer->getEmailIdentity(),
            $this->identityContainer->getStore()->getId()
        );
    }
}
